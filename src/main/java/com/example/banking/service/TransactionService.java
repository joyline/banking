package com.example.banking.service;

import com.example.banking.model.PaginationResponse;
import com.example.banking.model.SuccessResponse;
import com.example.banking.model.transaction.*;

public interface TransactionService {
    SuccessResponse topup(TopupRequest topupRequest, String basicAuth);

    SuccessResponse payment(PaymentRequest paymentRequest, String basicAuth);

    SuccessResponse transfer(TransferRequest transferRequest, String basicAuth);

    PaginationResponse getTransactionHistoryByAccountNumber(Integer page, Integer size, String accountNumber, String basicAuth);
}
