package com.example.banking.service;

import com.example.banking.config.exception.AppException;
import com.example.banking.entity.User;
import com.example.banking.model.*;
import com.example.banking.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.example.banking.config.RedisKey.REDIS_USER_SESSION;

@Service
public class UserServiceImpl implements UserService {
    final private String REGEX_EMAIL =
            "^[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,}$";

    private AccountService accountService;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private StringRedisTemplate stringRedisTemplate;
    private ObjectMapper objectMapper;

    @Autowired
    public UserServiceImpl(
            PasswordEncoder passwordEncoder,
            UserRepository userRepository,
            StringRedisTemplate stringRedisTemplate,
            AccountService accountService,
            ObjectMapper objectMapper
    ) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.stringRedisTemplate = stringRedisTemplate;
        this.objectMapper = objectMapper;
        this.accountService = accountService;
    }

    @Override
    public CreateUserResponse createUser(CreateUserRequest request) {
        try {
            validateCreateUserRequest(request);

            User userObject = new User();
            userObject.setEmail(request.email);
            userObject.setUsername(request.username);
            userObject.setPassword(passwordEncoder.encode(request.password));

            userRepository.saveAndFlush(userObject);

            CreateAccountRequest accountRequest = new CreateAccountRequest(userObject.getId());
            accountService.createAccount(accountRequest);

            CreateUserResponse response = new CreateUserResponse(userObject.getUsername(), userObject.getEmail());
            return response;
        } catch (Exception exception) {
            if (exception instanceof DataIntegrityViolationException) {
                throw new AppException("user data is duplicated");
            } else  {
                throw exception;
            }
        }
    }

    @Override
    public String findUserSessionByEmail(String email) {
        String redisKey = generateRedisKey(email);
        return stringRedisTemplate.opsForValue().get(redisKey);
    }

    @Override
    public User findUserByEmail(String email) {
        Optional<User> userObject = userRepository.findByEmail(email);
        return userObject.orElse(null);
    }

    @Override
    public User findUserByUserId(Integer userId) {
        Optional<User> userObject = userRepository.findById(userId);
        return userObject.orElse(null);
    }

    @Override
    public UserLoginResponse login(UserLoginRequest request) {
        String encoding = "Basic " + Base64.getEncoder().encodeToString((request.email + ":" + request.password).getBytes());
        Optional<User> userObject = userRepository.findByEmail(request.email);

        if (userObject.isEmpty()) {
            throw new AppException("user not found");
        }

        boolean isPasswordMatch = passwordEncoder.matches(request.password, userObject.get().getPassword());
        if (isPasswordMatch) {
            String redisKey = generateRedisKey(userObject.get().getEmail());
            stringRedisTemplate.opsForValue().set(redisKey, encoding, 8, TimeUnit.HOURS);
            return new UserLoginResponse(encoding);
        }
        else {
            throw new AppException("please check your email or password again");
        }
    }

    private String generateRedisKey(String email) {
        String redisKey = REDIS_USER_SESSION + ":" + email;
        return redisKey;
    }

    private void validateCreateUserRequest(CreateUserRequest request) {
        if (request.username.length() > 50) {
            throw new AppException("username length is more than 50 characters");
        }
        else if (request.password.length() > 30) {
            throw new AppException("password length is more than 30 characters");
        }
        else if (!request.email.matches(REGEX_EMAIL)) {
            throw new AppException("email format is invalid");
        }
    }
}
