package com.example.banking.service;

import com.example.banking.entity.Account;
import com.example.banking.model.CreateAccountRequest;
import com.example.banking.model.CreateAccountResponse;

public interface AccountService {
    CreateAccountResponse createAccount(CreateAccountRequest createAccountRequest);

    Account getAccountByUserId(Integer userId);

    Account getAccountByEmail(String email);

    Account getAccountByAccountNumber(String accountNumber);

    Account addAccountBalance(Integer userId, Integer amount);

    Account deductAccountBalance(Integer userId, Integer amount);
}
