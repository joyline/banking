package com.example.banking.service;

import com.example.banking.entity.Account;
import com.example.banking.model.CreateAccountRequest;
import com.example.banking.model.CreateAccountResponse;
import com.example.banking.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;

    @Autowired
    AccountServiceImpl(
            AccountRepository accountRepository
    ) {
        this.accountRepository = accountRepository;
    }

    @Override
    public CreateAccountResponse createAccount(CreateAccountRequest createAccountRequest) {
        String uniqueRandomNumber = generateUniqueRandomNumber();

        Account account = new Account();
        account.setBalance(0);
        account.setUserId(createAccountRequest.userId);
        account.setAccountNumber(uniqueRandomNumber);
        accountRepository.save(account);
        return null;
    }

    @Override
    public Account getAccountByUserId(Integer userId) {
        Optional<Account> accountObject = accountRepository.findByUserId(userId);
        return accountObject.orElse(null);
    }

    @Override
    public Account getAccountByEmail(String email) {
        return null;
    }

    @Override
    public Account getAccountByAccountNumber(String accountNumber) {
        Optional<Account> accountObject = accountRepository.findByAccountNumber(accountNumber);
        return accountObject.orElse(null);
    }

    @Override
    public Account addAccountBalance(Integer userId, Integer amount) {
        Optional<Account> accountObject = accountRepository.findByUserId(userId);
        if (accountObject.isEmpty()) {
            return null;
        }
        Integer currentBalance = accountObject.get().getBalance();
        Integer totalBalance = currentBalance + amount;

        accountObject.get().setBalance(totalBalance);
        accountRepository.saveAndFlush(accountObject.get());
        return accountObject.get();
    }

    @Override
    public Account deductAccountBalance(Integer userId, Integer amount) {
        Optional<Account> accountObject = accountRepository.findByUserId(userId);
        if (accountObject.isEmpty()) {
            return null;
        }
        Integer currentBalance = accountObject.get().getBalance();
        Integer totalBalance = currentBalance - amount;

        accountObject.get().setBalance(totalBalance);
        accountRepository.saveAndFlush(accountObject.get());
        return accountObject.get();
    }

    private String generateUniqueRandomNumber() {
        SecureRandom secureRandom = new SecureRandom();
        // Generate a random 10-digit number
        BigInteger random10DigitNumber = new BigInteger(10 * 4, secureRandom);

        // Ensure the generated number is positive and 10 digits long
        random10DigitNumber = random10DigitNumber.abs();
        random10DigitNumber = random10DigitNumber.mod(BigInteger.TEN.pow(10));

        // Convert the BigInteger to a string
        return String.format("%010d", random10DigitNumber);
    }
}
