package com.example.banking.service;

import com.example.banking.entity.User;
import com.example.banking.model.*;

public interface UserService {
    CreateUserResponse createUser(CreateUserRequest request);

    String findUserSessionByEmail(String email);

    User findUserByEmail(String email);

    User findUserByUserId(Integer userId);

    UserLoginResponse login(UserLoginRequest request);
}
