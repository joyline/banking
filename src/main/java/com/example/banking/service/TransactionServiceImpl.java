package com.example.banking.service;

import com.example.banking.config.exception.AppException;
import com.example.banking.entity.Account;
import com.example.banking.entity.Transaction;
import com.example.banking.entity.User;
import com.example.banking.model.PaginationResponse;
import com.example.banking.model.SuccessResponse;
import com.example.banking.model.transaction.*;
import com.example.banking.repository.TransactionRepository;
import com.example.banking.utilities.Helper;
import com.example.banking.utilities.TransactionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static com.example.banking.utilities.BankAccountConstant.*;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final Integer MINIMUM_AMOUNT = 10_000;

    private Helper helper;
    private AccountService accountService;
    private UserService userService;
    private TransactionRepository transactionRepository;

    @Autowired
    public TransactionServiceImpl(
            Helper helper,
            AccountService accountService,
            UserService userService,
            TransactionRepository transactionRepository
    ) {
        this.helper = helper;
        this.accountService = accountService;
        this.userService = userService;
        this.transactionRepository = transactionRepository;
    }

    // assuming can only topup your own account
    @Override
    public SuccessResponse topup(TopupRequest topupRequest, String basicAuth) {
        validateTopupRequest(topupRequest);

        List<String> basicAuthList = helper.parseBasicAuth(basicAuth);
        String email = basicAuthList.get(0);

        Account destinationAccount = accountService.getAccountByAccountNumber(topupRequest.accountNumber);
        User userData = userService.findUserByEmail(email);
        if (destinationAccount == null) {
            throw new AppException("bank account not found");
        } else if (userData == null) {
            throw new AppException("user not found");
        }

        String referenceNumber = generateTransactionReferenceNumber();
        Integer userId = userData.getId();

        LocalDateTime dateTime = LocalDateTime.now();
        Transaction transaction = new Transaction();
        transaction.setDestinationAccountName(userData.getUsername());
        transaction.setDestinationAccountNumber(destinationAccount.getAccountNumber());
        transaction.setSourceAccountName(TOPUP_ACCOUNT);
        transaction.setSourceAccountNumber(TOPUP_ACCOUNT_NUMBER);
        transaction.setTransactionAmount(topupRequest.amount);
        transaction.setCreatedAt(dateTime);

        transaction.setTransactionType(TransactionType.TOPUP.name());
        transaction.setReferenceNumber(referenceNumber);
        transactionRepository.saveAndFlush(transaction);

        Account account = accountService.addAccountBalance(userId, topupRequest.amount);
        if (account == null) {
            throw new AppException("fail to update account balance");
        }

        return new SuccessResponse("OK", "Top up success");
    }

    @Override
    public SuccessResponse payment(PaymentRequest paymentRequest, String basicAuth) {
        List<String> basicAuthList = helper.parseBasicAuth(basicAuth);
        String email = basicAuthList.get(0);

        Account sourceAccount = accountService.getAccountByAccountNumber(paymentRequest.accountNumber);
        User userData = userService.findUserByEmail(email);
        if (sourceAccount == null) {
            throw new AppException("bank account not found");
        } else if (userData == null) {
            throw new AppException("user not found");
        } else if (!Objects.equals(sourceAccount.getUserId(), userData.getId())) {
            throw new AppException("unable to perform this action");
        }

        String referenceNumber = generateTransactionReferenceNumber();
        Integer userId = userData.getId();

        LocalDateTime dateTime = LocalDateTime.now();
        Transaction transaction = new Transaction();
        transaction.setDestinationAccountName(PAYMENT_ACCOUNT);
        transaction.setDestinationAccountNumber(PAYMENT_ACCOUNT_NUMBER);
        transaction.setSourceAccountName(userData.getUsername());
        transaction.setSourceAccountNumber(sourceAccount.getAccountNumber());
        transaction.setTransactionAmount(paymentRequest.amount);
        transaction.setCreatedAt(dateTime);

        transaction.setTransactionType(TransactionType.PAYMENT.name());
        transaction.setReferenceNumber(referenceNumber);
        transactionRepository.saveAndFlush(transaction);

        Account account = accountService.deductAccountBalance(userId, paymentRequest.amount);
        if (account == null) {
            throw new AppException("fail to update account balance");
        }

        return new SuccessResponse("OK", "Payment success");
    }

    @Override
    public SuccessResponse transfer(TransferRequest transferRequest, String basicAuth) {
        if (transferRequest.sourceAccountNumber.equals(transferRequest.destinationAccountNumber)) {
            throw new AppException("please give proper input");
        }

        List<String> basicAuthList = helper.parseBasicAuth(basicAuth);
        String email = basicAuthList.get(0);


        Account sourceAccount = accountService.getAccountByAccountNumber(transferRequest.sourceAccountNumber);
        Account destinationAccount = accountService.getAccountByAccountNumber(transferRequest.destinationAccountNumber);

        if (sourceAccount == null || destinationAccount == null) {
            throw new AppException("account not found");
        }

        User sourceUserData = userService.findUserByEmail(email);
        User destinationUserData = userService.findUserByUserId(destinationAccount.getUserId());
        if (sourceUserData == null || destinationUserData == null) {
            throw new AppException("user not found");
        }
        else if (!Objects.equals(sourceAccount.getUserId(), sourceUserData.getId())) {
            throw new AppException("unable to perform this action");
        }

        String referenceNumber = generateTransactionReferenceNumber();

        LocalDateTime dateTime = LocalDateTime.now();
        Transaction transaction = new Transaction();
        transaction.setDestinationAccountName(destinationUserData.getUsername());
        transaction.setDestinationAccountNumber(destinationAccount.getAccountNumber());
        transaction.setSourceAccountName(sourceUserData.getUsername());
        transaction.setSourceAccountNumber(sourceAccount.getAccountNumber());
        transaction.setTransactionAmount(transferRequest.amount);
        transaction.setCreatedAt(dateTime);

        transaction.setTransactionType(TransactionType.TRANSFER.name());
        transaction.setReferenceNumber(referenceNumber);
        transactionRepository.saveAndFlush(transaction);

        accountService.deductAccountBalance(sourceAccount.getUserId(), transferRequest.amount);
        accountService.addAccountBalance(destinationAccount.getUserId(), transferRequest.amount);

        return new SuccessResponse("OK", "Transfer success");
    }

    @Override
    public PaginationResponse getTransactionHistoryByAccountNumber(Integer page, Integer size, String accountNumber, String basicAuth) {
        List<String> basicAuthList = helper.parseBasicAuth(basicAuth);
        String email = basicAuthList.get(0);

        User userData = userService.findUserByEmail(email);
        Account accountData = accountService.getAccountByUserId(userData.getId());

        if (!Objects.equals(accountData.getAccountNumber(), accountNumber)) {
            throw new AppException("unable to perform this action");
        }

        if (page < 1) {
            page = 1;
        }

        Page<Transaction> trxList = transactionRepository.findAllTransactionByAccountNumber(accountNumber, PageRequest.of(page - 1, size));

        List<TransactionHistoryResponse> mappedTrxList = trxList.stream()
                .map(transaction ->
                        new TransactionHistoryResponse(
                                transaction.getReferenceNumber(),
                                transaction.getTransactionType(),
                                transaction.getTransactionAmount(),
                                transaction.getSourceAccountName(),
                                transaction.getSourceAccountNumber(),
                                transaction.getDestinationAccountName(),
                                transaction.getDestinationAccountNumber(),
                                transaction.getCreatedAt() == null ? null : transaction.getCreatedAt().toLocalDate()
                        )
                )
                .toList();

        PaginationResponse response = new PaginationResponse();
        response.currentPage = page;
        response.totalPage = trxList.getTotalPages();
        response.totalData = trxList.getTotalElements();
        response.pagingData = mappedTrxList;
        return response;
    }

    private void validateTopupRequest(TopupRequest topupRequest) {
        if (topupRequest.amount < MINIMUM_AMOUNT) {
            throw new AppException("please topup more than " + MINIMUM_AMOUNT);
        }
    }

    private String generateTransactionReferenceNumber() {
        String referenceNumber = UUID.randomUUID().toString();
        return referenceNumber;
    }
}
