package com.example.banking.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {
    @JsonProperty(value = "status")
    public String status = "ERROR";

    @JsonProperty(value = "message")
    public String message = "";

    @JsonProperty(value = "debugInfo")
    public String debugInfo = "";

    public ErrorResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public ErrorResponse(String status, String message, String debugInfo) {
        this.status = status;
        this.message = message;
        this.debugInfo = debugInfo;
    }
}
