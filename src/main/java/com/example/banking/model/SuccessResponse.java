package com.example.banking.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SuccessResponse {

    @JsonProperty(value = "status")
    public String status = "OK";

    @JsonProperty(value = "message")
    public String message = "";

    public SuccessResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }
}
