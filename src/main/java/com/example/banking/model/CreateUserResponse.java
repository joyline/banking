package com.example.banking.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateUserResponse {
    @JsonProperty(value = "status")
    public String status = "OK";

    @JsonProperty(value = "username")
    public String username;

    @JsonProperty(value = "email")
    public String email;


    public CreateUserResponse(String username, String email) {
        this.username = username;
        this.email = email;
    }
}
