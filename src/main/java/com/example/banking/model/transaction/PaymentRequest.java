package com.example.banking.model.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class PaymentRequest {
    @NotNull(message = "amount cannot be null")
    @JsonProperty(value = "amount")
    public Integer amount;

    @NotNull(message = "accountNumber cannot be empty or blank or null")
    @NotBlank(message = "accountNumber cannot be empty or blank or null")
    @JsonProperty(value = "accountNumber")
    public String accountNumber;

    public PaymentRequest(Integer amount, String accountNumber) {
        this.amount = amount;
        this.accountNumber = accountNumber;
    }
}
