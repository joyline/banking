package com.example.banking.model.transaction;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

public class TransactionHistoryResponse {
    @JsonProperty(value = "referenceNumber")
    public String referenceNumber;

    @JsonProperty(value = "transactionType")
    public String transactionType;

    @JsonProperty(value = "amount")
    public Integer amount;

    @JsonProperty(value = "sourceAccountName")
    public String sourceAccountName;

    @JsonProperty(value = "sourceAccountNumber")
    public String sourceAccountNumber;

    @JsonProperty(value = "destinationAccountName")
    public String destinationAccountName;

    @JsonProperty(value = "destinationAccountNumber")
    public String destinationAccountNumber;

    @JsonProperty(value = "transactionDate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    public LocalDate transactionDate;

    public TransactionHistoryResponse() {}

    public TransactionHistoryResponse(
            String referenceNumber,
            String transactionType,
            Integer amount,
            String sourceAccountName,
            String sourceAccountNumber,
            String destinationAccountName,
            String destinationAccountNumber,
            LocalDate transactionDate
    ) {
        this.referenceNumber = referenceNumber;
        this.transactionType = transactionType;
        this.amount = amount;
        this.sourceAccountName = sourceAccountName;
        this.sourceAccountNumber = sourceAccountNumber;
        this.destinationAccountName = destinationAccountName;
        this.destinationAccountNumber = destinationAccountNumber;
        this.transactionDate = transactionDate;
    }
}
