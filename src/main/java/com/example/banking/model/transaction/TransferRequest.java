package com.example.banking.model.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class TransferRequest {
    @NotNull(message = "amount cannot be null")
    @JsonProperty(value = "amount")
    public Integer amount;

    @NotNull(message = "source accountNumber cannot be empty or blank or null")
    @NotBlank(message = "source accountNumber cannot be empty or blank or null")
    @JsonProperty(value = "sourceAccountNumber")
    public String sourceAccountNumber;

    @NotNull(message = "destination accountNumber cannot be empty or blank or null")
    @NotBlank(message = "destination accountNumber cannot be empty or blank or null")
    @JsonProperty(value = "destinationAccountNumber")
    public String destinationAccountNumber;

    public TransferRequest(Integer amount, String sourceAccountNumber) {
        this.amount = amount;
        this.sourceAccountNumber = sourceAccountNumber;
    }
}
