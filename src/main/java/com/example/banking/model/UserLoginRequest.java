package com.example.banking.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class UserLoginRequest {

    @NotNull(message = "email cannot be empty or blank or null")
    @NotBlank(message = "email cannot be empty or blank or null")
    @JsonProperty(value = "email")
    public String email;

    @NotNull(message = "password cannot be empty or blank or null")
    @NotBlank(message = "password cannot be empty or blank or null")
    @JsonProperty(value = "password")
    public String password;

}
