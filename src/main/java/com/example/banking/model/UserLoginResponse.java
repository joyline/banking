package com.example.banking.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserLoginResponse {
    @JsonProperty(value = "basicAuth")
    public String basicAuth;

    public UserLoginResponse(String basicAuth) {
        this.basicAuth = basicAuth;
    }
}
