package com.example.banking.model;

public class PaginationResponse {
    public Integer currentPage = 0;

    public Integer totalPage = 0;

    public Long totalData = 0L;

    public Object pagingData;

    public Object additionalData;

    public PaginationResponse() {

    }

    public PaginationResponse(Integer currentPage, Integer totalPage, Long totalData, Object pagingData, Object additionalData) {
        this.currentPage = currentPage;
        this.totalPage = totalPage;
        this.totalData = totalData;
        this.pagingData = pagingData;
        this.additionalData = additionalData;
    }
}
