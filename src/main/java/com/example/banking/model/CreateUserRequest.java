package com.example.banking.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class CreateUserRequest {
    @NotNull(message = "email cannot be empty or blank or null")
    @NotBlank(message = "email cannot be empty or blank or null")
    @JsonProperty(value = "email")
    public String email;

    @NotNull(message = "username cannot be empty or blank or null")
    @NotBlank(message = "username cannot be empty or blank or null")
    @JsonProperty(value = "username")
    public String username;

    @NotNull(message = "password cannot be empty or blank or null")
    @NotBlank(message = "password cannot be empty or blank or null")
    @JsonProperty(value = "password")
    public String password;


}
