package com.example.banking.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateAccountRequest {
    @JsonProperty(value = "userId")
    public Integer userId;

    public CreateAccountRequest(Integer userId) {
        this.userId = userId;
    }
}
