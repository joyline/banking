package com.example.banking.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateAccountResponse {
    @JsonProperty(value = "accountNumber")
    public String accountNumber;

    @JsonProperty(value = "balance")
    public Integer balance;

}
