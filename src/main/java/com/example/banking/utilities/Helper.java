package com.example.banking.utilities;

import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class Helper {

    public List<String> parseBasicAuth(String basicAuth) {
        String base64Credentials = basicAuth.substring("Basic ".length());
        byte[] decodedCredentials = Base64.getDecoder().decode(base64Credentials);
        String credentials = new String(decodedCredentials, StandardCharsets.UTF_8);

        String[] parts = credentials.split(":");
        return Arrays.stream(parts).toList();
    }
}
