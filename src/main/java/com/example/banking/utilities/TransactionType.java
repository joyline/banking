package com.example.banking.utilities;

public enum TransactionType {
    TOPUP,
    TRANSFER,
    PAYMENT;

    TransactionType() {
    }
}
