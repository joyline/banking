package com.example.banking.utilities;

public class BankAccountConstant {
    public static final String TOPUP_ACCOUNT = "TOPUP_ACCOUNT";
    public static final String TOPUP_ACCOUNT_NUMBER = "TOPUP_ACCOUNT_NUMBER";


    public static final String PAYMENT_ACCOUNT = "PAYMENT_ACCOUNT";
    public static final String PAYMENT_ACCOUNT_NUMBER = "PAYMENT_ACCOUNT_NUMBER";
}
