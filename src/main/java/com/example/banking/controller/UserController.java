package com.example.banking.controller;

import com.example.banking.model.CreateUserRequest;
import com.example.banking.model.CreateUserResponse;
import com.example.banking.model.UserLoginRequest;
import com.example.banking.model.UserLoginResponse;
import com.example.banking.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = {"/user"}, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserService userService;

    @Autowired
    UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("registration")
    public ResponseEntity<Object> postRegisterUser(
            @RequestBody @Valid CreateUserRequest request
    ) {
        CreateUserResponse response = userService.createUser(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("login")
    public ResponseEntity<Object> login(
            @RequestBody @Valid UserLoginRequest request
    ) {
        UserLoginResponse response = userService.login(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
