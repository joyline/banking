package com.example.banking.controller;

import com.example.banking.model.PaginationResponse;
import com.example.banking.model.SuccessResponse;
import com.example.banking.model.transaction.PaymentRequest;
import com.example.banking.model.transaction.TopupRequest;
import com.example.banking.model.transaction.TransferRequest;
import com.example.banking.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = {"/transaction"}, produces = MediaType.APPLICATION_JSON_VALUE)
public class TransactionController {

    private final TransactionService transactionService;

    @Autowired
    TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }


    @PostMapping("topup")
    public ResponseEntity<SuccessResponse> topup(
            @RequestBody TopupRequest request,
            @RequestHeader(HttpHeaders.AUTHORIZATION) String basicAuth
    ) {
        SuccessResponse response = transactionService.topup(request, basicAuth);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("payment")
    public ResponseEntity<Object> payment(
            @RequestBody PaymentRequest request,
            @RequestHeader(HttpHeaders.AUTHORIZATION) String basicAuth
    ) {
        SuccessResponse response = transactionService.payment(request, basicAuth);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("transfer")
    public ResponseEntity<Object> transfer(
            @RequestBody TransferRequest request,
            @RequestHeader(HttpHeaders.AUTHORIZATION) String basicAuth
    ) {
        SuccessResponse response = transactionService.transfer(request, basicAuth);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("history/{accountNumber}")
    public ResponseEntity<Object> trxHistory(
            @PathVariable String accountNumber,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
            @RequestHeader(HttpHeaders.AUTHORIZATION) String basicAuth
    ) {
        PaginationResponse response = transactionService.getTransactionHistoryByAccountNumber(page, size, accountNumber, basicAuth);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
