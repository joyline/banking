package com.example.banking.config.exception;

public class AppException extends RuntimeException {
    public AppException(String... defaultMessage) {
        super(String.join(",", defaultMessage));
    }
}
