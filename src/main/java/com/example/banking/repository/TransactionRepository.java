package com.example.banking.repository;

import com.example.banking.entity.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query(
            nativeQuery = true,
            value = "SELECT * FROM transaction t WHERE t.source_account_number = :accountNumber OR t.destination_account_number = :accountNumber ORDER BY t.created_at DESC",
            countQuery = "SELECT COUNT(*) FROM transaction t WHERE t.source_account_number = :accountNumber OR t.destination_account_number = :accountNumber"
    )
    Page<Transaction> findAllTransactionByAccountNumber(
            @Param("accountNumber") String accountNumber,
            Pageable pageable
    );
}
