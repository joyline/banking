package com.example.banking.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.time.LocalDateTime;

@Entity
@Table(name = "transaction")
@DynamicInsert
@DynamicUpdate
public class Transaction {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonIgnore
    private Long id;

    @Column(name = "reference_number")
    private String referenceNumber;

    @Column(name = "source_account_number")
    private String sourceAccountNumber;

    @Column(name = "source_account_name")
    private String sourceAccountName;

    @Column(name = "destination_account_number")
    private String destinationAccountNumber;

    @Column(name = "destination_account_name")
    private String destinationAccountName;

    @Column(name = "transaction_amount")
    private Integer transactionAmount;

    @Column(name = "transaction_type")
    private String transactionType;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public Transaction() {

    }

    public Transaction(String referenceNumber, String sourceAccountNumber, String sourceAccountName, String destinationAccountNumber, String destinationAccountName, Integer transactionAmount, String transactionType) {
        this.referenceNumber = referenceNumber;
        this.sourceAccountNumber = sourceAccountNumber;
        this.sourceAccountName = sourceAccountName;
        this.destinationAccountNumber = destinationAccountNumber;
        this.destinationAccountName = destinationAccountName;
        this.transactionAmount = transactionAmount;
        this.transactionType = transactionType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public void setSourceAccountNumber(String sourceAccountNumber) {
        this.sourceAccountNumber = sourceAccountNumber;
    }

    public String getSourceAccountName() {
        return sourceAccountName;
    }

    public void setSourceAccountName(String sourceAccountName) {
        this.sourceAccountName = sourceAccountName;
    }

    public String getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    public void setDestinationAccountNumber(String destinationAccountNumber) {
        this.destinationAccountNumber = destinationAccountNumber;
    }

    public String getDestinationAccountName() {
        return destinationAccountName;
    }

    public void setDestinationAccountName(String destinationAccountName) {
        this.destinationAccountName = destinationAccountName;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Integer transactionAmount) {
        this.transactionAmount = transactionAmount;
    }
}
