CREATE TABLE IF NOT EXISTS `account` (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `account_number` VARCHAR(255) UNIQUE,
    `user_id` INT,
    `balance` INT,
    `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` TIMESTAMP
);
