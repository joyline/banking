CREATE TABLE IF NOT EXISTS `transaction` (
   `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
   `reference_number` VARCHAR(255) NOT NULL,
   `source_account_number` VARCHAR(255),
   `source_account_name` VARCHAR(255),
   `destination_account_number` VARCHAR(255),
   `destination_account_name` VARCHAR(255),
   `transaction_amount` INT,
   `transaction_type` VARCHAR(150), -- PAYMENT, TOP UP, TRANSFER
   `created_at` timestamp
);
