package com.example.banking;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled
@SpringBootTest
class BankingApplicationTests {

	@Test
	void contextLoads() {
	}

}
