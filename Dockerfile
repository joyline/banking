# Use an official OpenJDK runtime as a base image
FROM openjdk:17-jdk-alpine

# Set the working directory in the container
WORKDIR /app

# Copy the Gradle build files and settings
COPY build.gradle .
COPY settings.gradle .
COPY gradlew .

# Copy the Gradle wrapper
COPY gradle/wrapper gradle/wrapper

# Download and install Gradle
RUN ./gradlew

# Copy the application source code
COPY src src

# Copy the application.properties file
COPY src/main/resources/application.properties /app/src/main/resources/

# Specify the command to run on container start
CMD ["java", "-jar", "build/libs/banking-0.0.1-SNAPSHOT.jar"]

# Expose the port that your application will run on
EXPOSE 8080
