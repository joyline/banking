# Banking App Project
#### This app is a simple prototype for banking application

---

## This is banking app built on top 
- Java 17
- Gradle-Groovy 
- Spring Boot 3.2.1
- Database MySQL 
- Cache Redis

### Recommended IDE
- IntelliJ IDEA


you can install manually, or using docker-compose to install all requirements.
```
docker-compose up -d
```
of if you want to install specific service
```
docker-compose up -d {service_name}
```

---
## How to Run
While using IntelliJ IDE, simply just click the Run Button.

---

## How to Build and Run Docker Image
Build the Docker image in the same directory as your Dockerfile
```
docker build -t {your-app-image} .
```

### Run the Docker container based on the built image
```
docker run -d -p 8080:8080 {your-app-image}
```

### Run on the same Docker network
This step is needed, If you have separate MySQL container, Redis container, and banking-app container.
```
docker network create {network-name}
docker run -d --name {mysql-container-name} --network {network-name} -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=banking_app -p 3306:3306 mysql
docker run -d --name {redis-container-name} --network {network-name} -p 6380:6379 redis:4.0.14
docker run --network {network-name} -e MYSQL_HOST={mysql-container-name} -e REDIS_HOST={redis-container-name} -p 8080:8080 --name {your-container-name} {your-app-image}
```
